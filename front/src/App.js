
import {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import Timer from './components/timer/Timer';


class App extends Component{

state = {
  contacts: []
}

//   async componentDidMount(){
//   try {
//     const response = await fetch('http://localhost:8000/contacts');
//     const contacts = await response.json() ;
//     this.setState({contacts})
//   } catch (e){
// console.log(e);
//   }

  async componentDidMount(){
    try{
          const response = await fetch('//localhost:8000/contacts')
          const result = await response.json()
          if(result.success){
            const contacts = result.result
            this.setState({contacts})
          }else{
            const error = result.message 
            this.setState({error})
          }
        }catch(err){
          this.setState({error_message:err})
        }
    }
    
//console.log("we got a response!!",contacts);



  render() {
let {contacts,error}=this.state;

return error ? (
  <p>{error}</p>) : (
  <div className="App">
    {/*<Timer timer={2}/>*/}
    {contacts.map(contacts =>(
      <div key={contacts.id}>
        <p>{contacts.name}</p>
        </div>
    ))}
    </div>
)
  }


// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }
}
export default App;
