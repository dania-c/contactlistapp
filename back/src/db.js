const sqlite = require("sqlite");
const sqlite3 = require("sqlite3");
const SQL = require("sql-template-strings");

const test = async () => {
  /*connect to the database*/
    const db = await sqlite.open({
        filename: 'db.sqlite',
        driver: sqlite3.Database /*because we re using express and sqlite*/
    });
  /**
   * Create the table
   **/ 
  await db.run(`CREATE TABLE contacts (contact_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, email text NOT NULL UNIQUE);`);
/**
   * let's insert a bit of data in it. We're going to insert 10 users
   * We first create a "statement"
   **/
 const stmt = await db.prepare(`INSERT INTO contacts (name, email) VALUES (?, ?)`);
 let i = 0;
 while(i<10){
   await stmt.run(`person ${i}`,`person${i}@server.com`);
   i++
 }
 /** finally, we close the statement **/
 await stmt.finalize();
/**
   * Then, let's read this data and display it to make sure everything works
   **/
 const rows = await db.all("SELECT contact_id AS id, name, email FROM contacts")
 rows.forEach( ({ id, name, email }) => console.log(`[id:${id}] - ${name} - ${email}`) )
}

const initializeDatabase = async () => {

  const db = await sqlite.open({
    filename: 'db.sqlite',
        driver: sqlite3.Database
  });
  /**
   * retrieves the contacts from the database
   */
  //  const getContactsList = async () => {
  //   //let returnString = ""
  //   const rows = await db.all("SELECT contact_id AS id, name, email FROM contacts")
  //   //rows.forEach( ({ id, name, email }) => returnString+=`[id:${id}] - ${name} - ${email}` )
  //   return rows
  // }

  /**
 * retrieves the contacts from the database
 * @param {string} orderBy an optional string that is either "name" or "email"
 * @returns {array} the list of contacts
 */
// db.all  to return the sql
//db.run for insert - update - delete
const getContactsList = async (orderBy) => {
  let statement = `SELECT contact_id AS id, name, email FROM contacts`
  switch(orderBy){
    case 'name': statement+= ` ORDER BY name`; break;
    case 'email': statement+= ` ORDER BY email`; break;
    default: break;
  }
  try {
    const rows = await db.all(statement)
    if(!rows.length) throw new Error(`no rows found`);
    return rows
  } catch(err){
    throw new Error(`couldn't retrieve contacts: `+err.message)
  }
}

const getContact = async (id) => {
 let statment =`SELECT contact_id AS id, name, email FROM contacts WHERE contact_id = ${id}`
   const contact = await db.get(statment)
   if(!contact) throw new Error(`contact ${id} not found`);

  return contact
}
 


const createContact = async (props) => {
  if(!props || !props.name || !props.email){
    throw new Error(`you must provide a name and an email`)
  }

  const { name, email } = props
 try{
  const result = await db.run(`INSERT INTO contacts (name,email) VALUES (?, ?)`,[name,email]);
  const id = result.lastID
  return id}
  catch(err){
    throw new Error(`couldn't insert this combination: `+err.message);
  }
}

const deleteContact = async (id) => {
  try {
  const result = await db.run(`DELETE FROM contacts WHERE contact_id = ?`,id);
  
  if(result.stmt.changes === 0){
    throw new Error(`contact "${id}" does not exist`)
  }
  return true

} catch (err){
  throw new Error(`couldn't delete the contact "${id}": `+err.message);
}
}

const updateContact = async (id, props) => {
  if (!props && !(props.name && props.email)) {
    throw new Error(`you must provide a name or an email`);
  }

  
  const { name, email } = props;
let stmt, params =[];

if(name && email)
{ 
  stmt = `UPDATE contacts SET email=?, name=? WHERE contact_id = ?`;
  params =[email,name,id];
 
}
else if(name && !email)
{ 
  stmt = `UPDATE contacts SET name=? WHERE contact_id = ?`;
  params =[name,id];
}
else if(email && !name)
{
  stmt = `UPDATE contacts SET email=? WHERE contact_id = ?`;
  params =[email,id];
}
  
try {
  const result = await db.run(stmt,params);
  if (result.stmt.changes === 0) {
    throw new Error(`no changes were made`);
  }
  return true;


} catch(err) {
  throw new Error(`couldn't update the contact ${id}: ` + err.message);

}
}

  const controller = {
    getContactsList,  // ES6:like myval : getContactsList; but because they have the same name we write just the name of the function
    getContact,
    createContact,
    deleteContact,
    updateContact

  }

  return controller;
}


module.exports = { initializeDatabase };

