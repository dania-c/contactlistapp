const app = require('./app');
import db from './db';



const start = async () => {
    const controller = await db.initializeDatabase()
    app.get('/', (req, res) => res.send("ok"));
    

    
    // CREATE
  app.get('/contacts/create', async (req, res, next) => {
    const { name, email } = req.query
    const result = await controller.createContact({name,email})
    res.json({success:true, result})
  })

  // READ single
  app.get('/contacts/:id', async (req, res, next) => {
    const { id } = req.params
    const contact = await controller.getContact(id)
    res.json({success:true, result:contact})
  })

// DELETE
  app.get('/contacts/delete/:id', async (req, res, next) => {
    const { id } = req.params
    const result = await controller.deleteContact(id)
    res.json({success:true, result})
  })
  
  // UPDATE
  app.get('/contacts/update/:id', async (req, res, next) => {
    const { id } = req.params
    const { name, email } = req.query
    const result = await controller.updateContact(id,{name,email})
    res.json({success:true, result})
  })

  // LIST
  app.get('/contacts', async (req, res, next) => {
    const { orderBy } = req.query;
    const contacts = await controller.getContactsList(orderBy);
    res.json({success:true, result:contacts});
  })

  app.use((err, req, res, next) => {
    console.error(err);
    const message = err.message;
    res.status(500).json({ success:false, message });
  })


    //  let id = 17;
    //   // const id = await controller.createContact({name:"Brad 5Putt",email:"brad9@pet.com"})
    //   // const contact = await controller.getContact(id)
    //   // console.log("------\nmy newly created contact\n",contact)}
      
    //  await controller.updateContact(id, {name:" new17 Brad Pitt"})
    //  id=18;
    //   await controller.updateContact(id, {email:"newbrad18@pitt.com"})
    //   id=15;
    //   await controller.updateContact(id, {name:" new15 Brad Pitt",email:"newbrad15@pitt.com"})
    //   const updated_contact = await controller.getContact(id)
    //   console.log("------\nmy updated contact\n",updated_contact)
    //   console.log("------\nlist of contacts before\n",await controller.getContactsList())
    //   await controller.deleteContact(id)
    //   console.log("------\nlist of contacts after deleting\n",await controller.getContactsList())
      
    }
    

  
start();



app.listen( 8000, () => console.log('server listening on port 8000') );

